 # Auto - Window commandline Automation tool

<p align="center">
  <img width="25%" height="25%" src="https://github.com/marcuwynu23/Auto/blob/main/docs/images/1.jpg" />
   <img width="25%" height="25%" src="https://github.com/marcuwynu23/Auto/blob/main/docs/images/2.jpg" />
</p>
Commandline Tool use to automate commandline execution and instance creation in window terminal

How to use:
Create a file .autofile then add commands like below in the .autofile:
```
# this is a comment
- echo hello world!
- echo hi friend
```

then run in terminal:
```
auto
```

## NOTE:
Once the commands inside the .autofile is already RUNNING no need to rerun again the auto command sometimes it can cause error to the commands execution
specially if the a specify command can only run in ONE INSTANCE.


<!-- CONTRIBUTING -->
## Contributing
How to Contribute to this Project Repository:
1. Fork the Project
2. Create your Branch 
3. Commit your Changes 
4. Push to the Branch 
5. Open a Pull Request

Thank you in advance for your contributions! Godbless and Happy Coding! 
